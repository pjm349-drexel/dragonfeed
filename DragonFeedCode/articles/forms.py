from django.forms import ModelForm
from .models import Article

class AddArticle(ModelForm):
    class Meta:
        model = Article
        fields = ['Title', 'Orginization', 'Location', 'Description', 'Start', 'End', 'Tag', 'Link', 'Flyer']