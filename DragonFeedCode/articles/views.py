from .models import Article
from django.views import generic
from django.views.generic import View
from django.views.generic.edit import  CreateView, UpdateView, DeleteView
from django.http import HttpResponse

class IndexView(generic.ListView):
    template_name = 'index.html'
    context_object_name = 'allArticles'

    def get_queryset(self):
        return Article.objects.all()

class FormalEventsView(generic.ListView):
    template_name = 'formalEvents.html'
    context_object_name = 'allArticles'
    
    def get_queryset(self):
        return Article.objects.all()

class InformalEventsView(generic.ListView):
    template_name = 'informalEvents.html'
    context_object_name = 'allArticles'
    
    def get_queryset(self):
        return Article.objects.all()
    
class DetailView(generic.DetailView):
    template_name = 'details.html'
    model = Article

class AddEvent(CreateView):
    model = Article
    fields = '__all__'
    template_name = 'article_form.html'
    fields = ['Title', 'Orginization', 'Location', 'Description', 'Start', 'End', 'Tag', 'Link', 'Flyer']

