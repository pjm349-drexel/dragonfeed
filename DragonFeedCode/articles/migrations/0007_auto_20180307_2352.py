# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-03-07 23:52
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0006_article'),
    ]

    operations = [
        migrations.RenameField(
            model_name='article',
            old_name='tag',
            new_name='Tag',
        ),
    ]
