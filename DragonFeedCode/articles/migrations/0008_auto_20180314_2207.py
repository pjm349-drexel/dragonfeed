# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-03-14 22:07
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0007_auto_20180307_2352'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='article',
            name='Description',
        ),
        migrations.RemoveField(
            model_name='article',
            name='End',
        ),
        migrations.RemoveField(
            model_name='article',
            name='Flyer',
        ),
        migrations.RemoveField(
            model_name='article',
            name='Link',
        ),
        migrations.RemoveField(
            model_name='article',
            name='Location',
        ),
        migrations.RemoveField(
            model_name='article',
            name='Orginization',
        ),
        migrations.RemoveField(
            model_name='article',
            name='Start',
        ),
        migrations.RemoveField(
            model_name='article',
            name='Tag',
        ),
    ]
